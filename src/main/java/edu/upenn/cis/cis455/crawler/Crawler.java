package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.DocBolt;
import edu.upenn.cis.stormlite.bolt.MatcherBolt;
import edu.upenn.cis.stormlite.bolt.ParserBolt;
import edu.upenn.cis.stormlite.bolt.UrlBolt;
import edu.upenn.cis.stormlite.spout.CrawlerSpout;
import edu.upenn.cis.stormlite.tuple.Fields;

public class Crawler implements CrawlMaster {
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.

    static final int NUM_WORKERS = 10;
    private ThreadPoolExecutor executorPool;
    private String startUrl;
    private StorageInterface db;
    private int maxSize;
    private int maxDocs;
    private int numDocs;
    private Map<String, List<String>> disallow;
	private Map<String, Integer> delay;
	private Map<String, Long> lastAccessed;
	
	private Config config;
	private LocalCluster cluster;
	private Topology topo;
	private BlockingQueue<String> urls;

    private static final String CRAWLER_SPOUT = "CRAWLER_SPOUT";
	private static final String DOC_BOLT = "WORD_SPOUT";
    private static final String PARSER_BOLT = "PRINT_BOLT";
    private static final String MATCHER_BOLT = "MATCHER_BOLT";
    
    private XPathEngine xpath;
    private List<String> channelNamesList;
    
    private static Crawler cm;
    private int activeWorkers;

    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        // TODO: initialize
    	this.startUrl = startUrl;
    	this.db = db;
    	this.maxSize = size;
    	this.maxDocs = count;
    	this.disallow = new HashMap<String, List<String>>();
    	this.delay = new HashMap<String, Integer>();
    	this.lastAccessed = new HashMap<String, Long>();
    	this.activeWorkers = 0;
//    	ThreadFactory threadFactory = Executors.defaultThreadFactory();
//    	this.executorPool = new ThreadPoolExecutor(2, NUM_WORKERS, 10, TimeUnit.SECONDS, 
//    			new ArrayBlockingQueue<Runnable>(1000), threadFactory);
    	this.urls = new ArrayBlockingQueue<String>(1000);
        
        // initialize channels into xpath
        this.xpath = XPathEngineFactory.getXPathEngine();
        this.channelNamesList = new ArrayList<String>();
        Set<String> channelNames = this.db.getChannelNames();
        for (String s: channelNames) {
        	String path = this.db.getChannel(s).getPath();
        	this.xpath.setXPaths(s, path);
        	this.channelNamesList.add(s);
        }
    	
    	this.config = new Config();
    	CrawlerSpout crawlerSpout = new CrawlerSpout(this);
    	DocBolt docBolt = new DocBolt(this);
    	ParserBolt parserBolt = new ParserBolt(this);
    	MatcherBolt matcherBolt = new MatcherBolt(this);
    	
    	TopologyBuilder builder = new TopologyBuilder();
    	
    	builder.setSpout(CRAWLER_SPOUT, crawlerSpout, 1);
    	
    	builder.setBolt(DOC_BOLT, docBolt, 1).shuffleGrouping(CRAWLER_SPOUT);
    	builder.setBolt(PARSER_BOLT, parserBolt, 1).shuffleGrouping(DOC_BOLT);
    	builder.setBolt(MATCHER_BOLT, matcherBolt, 1).fieldsGrouping(PARSER_BOLT, new Fields("url"));

    	this.cluster = new LocalCluster();
        this.topo = builder.createTopology();
        this.cm = this;
    }
    
    public static Crawler getCrawler() {
    	if (cm == null) {
    		cm = new Crawler(null, null, 0, 0);
    	}
    	return cm;
    }
    
    public List<String> getChannelNamesList() {
    	return this.channelNamesList;
    }

    /**
     * Main thread
     */
    public void start() {
//    	this.executorPool.execute(new CrawlerWorker(this, this.startUrl));
		this.urls.add(this.startUrl);
        ObjectMapper mapper = new ObjectMapper();
		try {
			String str = mapper.writeValueAsString(topo);

			System.out.println("The StormLite topology is:\n" + str);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
        cluster.submitTopology("start", config, topo);
    }
    
    public Map<String, List<String>> getDisallow() {
    	return this.disallow;
    }
    
    public Map<String, Integer> getDelay() {
    	return this.delay;
    }
    
    public Map<String, Long> getLastAccessed() {
    	return this.lastAccessed;
    }
    
    public int getMaxSize() {
    	return this.maxSize;
    }
    
    public int getMaxDocs() {
    	return this.maxDocs;
    }
    
    public int getNumDocs() {
    	return this.numDocs;
    }
    
    public ThreadPoolExecutor getExecutorPool() {
    	return this.executorPool;
    }
    
    public StorageInterface getDb() {
    	return this.db;
    }
    
    public BlockingQueue<String> getUrls() {
    	return this.urls;
    }

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
    	this.numDocs++;
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
//        return (this.executorPool.getPoolSize() == 0 && this.executorPool.getActiveCount() == 0)
//        		|| this.numDocs >= this.maxDocs;
    	return (this.urls.size() == 0 && this.activeWorkers == 0)
        		|| this.numDocs >= this.maxDocs;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    	if (working) {
    		this.activeWorkers += 1;
    	} else {
    		this.activeWorkers -= 1;
    	}
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
        
        Crawler crawler = new Crawler(startUrl, db, size, count);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();

        while (!crawler.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        // TODO: final shutdown

        System.out.println("Done crawling!");
    }

}
