package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Response;
import spark.Route;

public class ChannelHandler implements Route{
	StorageInterface db;

    public ChannelHandler(StorageInterface db) {
        this.db = db;
    }
    
	@Override
	public Object handle(Request req, Response res) throws Exception {
		System.out.println("hi");
		String name = req.params(":name");
		String xpath = req.queryParams("xpath");
		String creator = req.session().attribute("user");
		int id = this.db.addChannel(name, creator, xpath);
		if (id == -1) {
			res.status(404);
			return null;
		}
		// add name -> path to channel map
		return "<html><a href=\"/index.html\">Homepage</a></html>";
	}

}
