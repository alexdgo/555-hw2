package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class RegistrationHandler implements Route {
	StorageInterface db;

    public RegistrationHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        System.err.println("Registration request for " + user + " and " + pass);
        // check if user or pass are already in db
        int id = db.addUser(user, pass);
        if (id == -1) {
        	System.err.println(user + " is already in the database");
            resp.redirect("/login-form.html");
        }
        resp.status(200);
        resp.type("text/html");
        Session session = req.session();

        session.attribute("user", user);
        session.attribute("password", pass);
        session.maxInactiveInterval(300);
        return "<html><a href=\"/index.html\">Homepage</a></html>";
    }
}
