package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class LogoutHandler implements Route {
	StorageInterface db;

    public LogoutHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {

        System.err.println("Logout request for ");
        Session session = req.session(false);
        if (session != null) {
        	session.invalidate();
        }
        resp.redirect("/login-form.html");

        return "";
    }
}
