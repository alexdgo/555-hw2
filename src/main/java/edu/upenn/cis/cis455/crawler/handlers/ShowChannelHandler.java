package edu.upenn.cis.cis455.crawler.handlers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.cis455.storage.ChannelData;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.UrlData;
import spark.Request;
import spark.Response;
import spark.Route;

public class ShowChannelHandler implements Route{
	StorageInterface db;

    public ShowChannelHandler(StorageInterface db) {
        this.db = db;
    }
    
	@Override
	public Object handle(Request req, Response res) throws Exception {
		String channel = req.queryParams("channel");
		if(!this.db.containsChannel(channel)) {
			res.status(404);
			return "<html><body><h2>404 Not Found</h2></body></html>";
		}
		System.out.println(channel);
		ChannelData c = this.db.getChannel(channel);
		System.out.println(c.getCreator());
		List<String> docs = this.db.getChannelSites(channel);
		System.out.println(docs);
		// c = this.db.getChannel(channel);
		String out = "<html><head><title>" + channel + "</title></head><body>";
		out += "<div class =\"channelheader\">Channel Name: " + channel
				+ ", created by: " + c.getCreator() + "</div>";
		for(String url : docs) {
			DateFormat sdf = new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss");
			out += "Crawled on: " + sdf.format(this.db.getLastModified(url));
			out += "Location: " + url;
			out += "<div class=\"document\">" + this.db.getDocument(url) + "</div>";
		}
		out += "</body></html>";
		System.out.println("made out");
		return out;
	}

}
