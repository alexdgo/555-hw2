package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;

public class LookupHandler implements Route{
	private StorageInterface db;
	public LookupHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
    public String handle(Request req, Response resp) throws HaltException {
		String url = req.queryParams("url");
		String body = this.db.getDocument(url);
		if (body == null) {
			return "<html><body>404 not found</body></html>";
		}
		return body;
    }
}
