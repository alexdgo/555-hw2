package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;

public class CrawlerWorker implements Runnable {
	private final String url;
	private final Crawler cm;
	private Map<String, List<String>> disallow;
	private Map<String, Integer> delay;
	
	public CrawlerWorker(Crawler cm, String url) {
		this.url = url;
		this.cm = cm;
		this.disallow = this.cm.getDisallow();
		this.delay = this.cm.getDelay();
	}
	
	public List<String> getDisallow(String url) {
		return this.disallow.get(url);
	}
	
	private String getText(String url) throws IOException {
	    HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
	    
	    // handle error response code it occurs
	    int responseCode = connection.getResponseCode();
	    InputStream inputStream;
	    if (200 <= responseCode && responseCode <= 299) {
	        inputStream = connection.getInputStream();
	    } else {
	        inputStream = connection.getErrorStream();
	    }

	    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

	    StringBuilder response = new StringBuilder();
	    String currentLine;
	    boolean notFirstLine = false;
	    while ((currentLine = in.readLine()) != null) {
	    	if (notFirstLine) response.append("\r\n");
	    	response.append(currentLine);
	    	notFirstLine = true;
	    }
	    in.close();
	    return response.toString();
	}
	
	public String robotsUrl(String url) {
		URLInfo urlinfo = new URLInfo(url);
		return urlinfo.isSecure() ? "https://" + urlinfo.getHostName() + "/robots.txt"
				: "http://" + urlinfo.getHostName() + "/robots.txt";
	}
	
	public void robots(String url){
		URLInfo urlinfo = new URLInfo(url);
		try {
			String newUrl = robotsUrl(url);
			if (this.disallow.containsKey(newUrl) || this.delay.containsKey(newUrl)) {
				return;
			}
			HttpURLConnection connection = (HttpURLConnection) 
					new URL(newUrl).openConnection();
			InputStream inputStream = connection.getInputStream();
		    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		    StringBuilder response = new StringBuilder();
		    String currentLine;
		    boolean read = false;
		    ArrayList<String> dis = new ArrayList<String>();
		    while ((currentLine = in.readLine()) != null) {
		    	if (currentLine.equals("User-agent: *") 
		    			|| currentLine.equals("User-agent: cis455crawler")) {
		    		read = true;
		    	}
		    	if (read && currentLine.startsWith("Disallow:")) {
		    		dis.add(currentLine.substring(10));
		    	}
		    	if (read && currentLine.startsWith("Crawl-delay:")) {
		    		this.delay.put(newUrl, Integer.parseInt(currentLine.substring(13)));
		    	}
		    	if (read && currentLine.startsWith("User-agent:") &&
		    			!(currentLine.equals("User-agent: *") 
				    	|| currentLine.equals("User-agent: cis455crawler"))) {
		    		read = false;
		    	}
		    	response.append(currentLine);
		    }
		    this.disallow.put(newUrl, dis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void run() {
		try {
			// Check num files retrieved
			if (this.cm.getNumDocs() >= this.cm.getMaxDocs()) return;

			// check size in mb
			URL url = new URL(this.url);
			HttpURLConnection urlc = (HttpURLConnection)url.openConnection();
			urlc.setRequestProperty("User-Agent", "cis455crawler");
			urlc.setRequestMethod("HEAD");
			long fileSize = urlc.getContentLength();
			if (urlc.getContentLength() > 0 && fileSize > (long)this.cm.getMaxSize() * 1024*1024) {
				return;
			}
			
			// check robots.txt
			this.robots(this.url);
			URLInfo urlinfo = new URLInfo(this.url);
			String path = urlinfo.getFilePath();
			if (this.disallow.containsKey(this.robotsUrl(this.url))) {
				for (String s: this.disallow.get(this.robotsUrl(this.url))) {
					if (path.startsWith(s)) return;
				}
			}
			//last accessed less than crawl-delay seconds ago
			String newUrl = this.robotsUrl(this.url);
			if (this.cm.getLastAccessed().containsKey(newUrl)) {
				int lastAccess = (int) (System.currentTimeMillis() - this.cm.getLastAccessed()
						.get(newUrl))/1000;
				if (this.delay.containsKey(newUrl) && lastAccess < this.delay.get(newUrl) ) {
					this.cm.getExecutorPool().execute(new CrawlerWorker(cm, this.url));
					return;
				}
			}
			this.cm.getLastAccessed().put(this.robotsUrl(this.url), System.currentTimeMillis());
			
			String body = getText(this.url);
			// add to db
			int bodyId = this.cm.getDb().addDocument(this.url, body);
			// check if content has been seen already
			if (bodyId == -1) return;
			
			this.cm.incCount();
			if (urlc.getContentType().contains("text/html")) {
				// bfs
				Document doc = Jsoup.connect(url.toString()).get();
				Elements links = doc.select("a[href]");
				for (Element link: links) {
					this.cm.getExecutorPool().execute(new CrawlerWorker(cm, link.attr("abs:href")));
				}
			}
		} catch (MalformedURLException e) {
			System.err.println("Invalid URL");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Can not open URL");
			e.printStackTrace();
		}
	}
	
}
