package edu.upenn.cis.cis455.storage;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserData implements Serializable {
	private String passHash;
	private int id;
	
	public UserData(String password, int id) {
		this.id = id;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
		    BigInteger bi = new BigInteger(1, md.digest());
			this.passHash = bi.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	public String getPassHash() {
		return this.passHash;
	}
	
	public int getId() {
		return this.id;
	}
}
