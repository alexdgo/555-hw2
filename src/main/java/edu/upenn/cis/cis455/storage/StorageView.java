package edu.upenn.cis.cis455.storage;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;

public class StorageView {
	private StoredSortedMap userMap;
	private StoredSortedMap urlMap;
	private StoredSortedMap contentSeenMap;
	
	public StorageView(StorageImpl db) {
		ClassCatalog catalog = db.getClassCatalog();
		EntryBinding userKeyBinding = new SerialBinding(catalog, String.class);
        EntryBinding userDataBinding = new SerialBinding(catalog, String.class);
        EntryBinding urlKeyBinding = new SerialBinding(catalog, String.class);
        EntryBinding urlDataBinding = new SerialBinding(catalog, UrlData.class);
        EntryBinding contentSeenKeyBinding = new SerialBinding(catalog, String.class);
        EntryBinding contentSeenDataBinding = new SerialBinding(catalog, String.class);
        
        this.userMap = new StoredSortedMap(db.getUserDatabase(), 
        		userKeyBinding, userDataBinding, true);
        this.urlMap = new StoredSortedMap(db.getUrlDatabase(), 
        		urlKeyBinding, urlDataBinding, true);
        this.contentSeenMap = new StoredSortedMap(db.getContentSeenDatabase(),
        		contentSeenKeyBinding, contentSeenDataBinding, true);
	}
	public final StoredSortedMap getUserMap()
    {
        return this.userMap;
    }

    public final StoredEntrySet getUserEntrySet()
    {
        return (StoredEntrySet) this.userMap.entrySet();
    }
    
    public final StoredSortedMap getUrlMap()
    {
        return this.urlMap;
    }

    public final StoredEntrySet getUrlEntrySet()
    {
        return (StoredEntrySet) this.urlMap.entrySet();
    }
    
    public final StoredSortedMap getContentSeenMap()
    {
        return this.contentSeenMap;
    }

    public final StoredEntrySet getContentSeenEntrySet()
    {
        return (StoredEntrySet) this.contentSeenMap.entrySet();
    }
}
