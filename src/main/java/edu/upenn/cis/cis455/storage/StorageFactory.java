package edu.upenn.cis.cis455.storage;

public class StorageFactory {
	static StorageInterface users;
	protected StorageFactory() {}
    public static StorageInterface getDatabaseInstance(String directory) {
        // TODO: factory object, instantiate your storage server
    	if (users == null) {
    		users = new StorageImpl(directory);
    	}
        return users;
    }
}
