package edu.upenn.cis.cis455.storage;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UrlData implements Serializable {
	private String bodyHash;
	private long lastModified;
	private int contentLength;
	private String body;
	private int id;
	
	public UrlData(String body, int id) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(body.getBytes());
		    byte[] digest = md.digest();
		    BigInteger bi = new BigInteger(1, md.digest());
			this.bodyHash = bi.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		this.body = body;
		this.contentLength = this.body.length();
		this.id = id;
		this.lastModified = System.currentTimeMillis();
	}
	
	public final String getBodyHash() {
		return this.bodyHash;
	}
	
	public final long getLastModified() {
		return this.lastModified;
	}
	
	public final int contentLength() {
		return this.contentLength;
	}
	
	public final String getBody() {
		return this.body;
	}
	
	public final int getId() {
		return this.id;
	}
}
