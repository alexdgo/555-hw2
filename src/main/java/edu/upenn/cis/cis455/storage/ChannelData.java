package edu.upenn.cis.cis455.storage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ChannelData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7832524167301640655L;
	private String creator;
	private String path;
	private int id;
	private List<String> docs;
	private Set<String> added;
	
	public ChannelData(String creator, String path, int id) {
		this.creator = creator;
		this.path = path;
		this.id = id;
		this.docs = new ArrayList<String>();
		this.added = new HashSet<String>();
	}
	
	public int addDoc(String url) {
		if (this.added.contains(url)) {
			return -1;
		}
		this.added.add(url);
		this.docs.add(url);
//		System.out.println(url);
		System.out.println(path + " " + this.docs);
		return this.docs.size();
	}
	
	public String getCreator() {
		return this.creator;
	}
	
	public List<String> getDocs() {
		return this.docs;
	}
	
	public String getPath() {
		return this.path;
	}
}
