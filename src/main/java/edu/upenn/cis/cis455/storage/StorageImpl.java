package edu.upenn.cis.cis455.storage;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class StorageImpl implements StorageInterface {
	static final Logger logger = Logger.getLogger(StorageImpl.class.getName());
	private Environment env;
	
	private static final String CLASS_CATALOG = "java_class_catalog";
	private static final String USER_STORE = "user_store";
    private static final String URL_STORE = "url_store";
    private static final String CONTENT_SEEN_STORE = "content_seen_store";
    private static final String CHANNEL_STORE = "channel_store";
    private static final String CHANNEL_SITES_STORE = "channel_sites_store";
    
    private Database userDb;
    private Database urlDb;
    private Database contentSeenDb;
    private Database channelDb;
    private Database channelSitesDb;
    
    private StoredSortedMap<String, UserData> userMap;
    private StoredSortedMap<String, UrlData> urlMap;
    private StoredSortedMap<String, Integer> contentSeenMap;
    private StoredSortedMap<String, ChannelData> channelMap;
    private StoredSortedMap<String, Set<String>> channelSitesMap;
    
    
	
	private StoredClassCatalog javaCatalog;
	
	public StorageImpl(String directory) {
		System.out.println("Opening environment in: " + directory);

        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);

        env = new Environment(new File(directory), envConfig);
        
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        
        this.userDb = this.env.openDatabase(null, USER_STORE, dbConfig);
        this.urlDb = this.env.openDatabase(null, URL_STORE, dbConfig);
        this.contentSeenDb = this.env.openDatabase(null, CONTENT_SEEN_STORE, dbConfig);
        this.channelDb = this.env.openDatabase(null, CHANNEL_STORE, dbConfig);
        this.channelSitesDb = this.env.openDatabase(null, CHANNEL_SITES_STORE, dbConfig);

        Database catalogDb = env.openDatabase(null, CLASS_CATALOG, 
                                              dbConfig);

        javaCatalog = new StoredClassCatalog(catalogDb);
        
        EntryBinding userKeyBinding = new SerialBinding(this.javaCatalog, String.class);
        EntryBinding userDataBinding = new SerialBinding(this.javaCatalog, UserData.class);
        EntryBinding urlKeyBinding = new SerialBinding(this.javaCatalog, String.class);
        EntryBinding urlDataBinding = new SerialBinding(this.javaCatalog, UrlData.class);
        EntryBinding contentSeenKeyBinding = new SerialBinding(this.javaCatalog, String.class);
        EntryBinding contentSeenDataBinding = new SerialBinding(this.javaCatalog, Integer.class);
        EntryBinding channelKeyBinding = new SerialBinding(this.javaCatalog, String.class);
        EntryBinding channelDataBinding = new SerialBinding(this.javaCatalog, ChannelData.class);
        EntryBinding channelSitesKeyBinding = new SerialBinding(this.javaCatalog, String.class);
        EntryBinding channelSitesDataBinding = new SerialBinding(this.javaCatalog, Set.class);
        
        
        this.userMap = new StoredSortedMap(this.userDb, userKeyBinding, userDataBinding, true);
        this.urlMap = new StoredSortedMap(this.urlDb, urlKeyBinding, urlDataBinding, true);
        this.contentSeenMap = new StoredSortedMap(this.contentSeenDb,
        		contentSeenKeyBinding, contentSeenDataBinding, true);
        this.channelMap = new StoredSortedMap(this.channelDb, channelKeyBinding, channelDataBinding, true);
        this.channelSitesMap = new StoredSortedMap(this.channelSitesDb, 
        		channelSitesKeyBinding, channelSitesDataBinding, true);
        
        this.contentSeenMap.clear();
	}
	
	public String getMd5Hash(String body) {
		String bodyHash = "";
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(body.getBytes());
		    BigInteger bi = new BigInteger(1, md.digest());
			bodyHash = bi.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return bodyHash;
	}
	
	public final Database getUserDatabase() {
		return this.userDb;
	}
	
	public final Database getUrlDatabase() {
		return this.urlDb;
	}
	
	public final Database getContentSeenDatabase() {
		return this.contentSeenDb;
	}
	
	public final Environment getEnvironment() {
		return this.env;
	}
	
	public final StoredClassCatalog getClassCatalog() {
        return this.javaCatalog;
    } 

	@Override
	public int getCorpusSize() {
		return this.urlMap.size();
	}

	@Override
	public int addDocument(String url, String documentContents) {
		String bodyHash = this.getMd5Hash(documentContents);
		long lastModified = 0;
		try {
			HttpURLConnection urlc = (HttpURLConnection)(new URL(url)).openConnection();
			lastModified = urlc.getLastModified();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// check seen before
		int docId = this.urlMap.size();
		if (this.contentSeenMap.containsKey(bodyHash)) {
			return -1;
		}
		if (this.urlMap.containsKey(url)) {
			docId = this.urlMap.get(url).getId();
		}
		this.contentSeenMap.put(bodyHash, docId);
		if (this.urlMap.containsKey(url) && lastModified <= this.urlMap.get(url).getLastModified()) {
			logger.info(url + ":not modified");
			return -2;
		}
		logger.info(url+":downloading");
		this.urlMap.put(url, new UrlData(documentContents, docId));
		return docId;
	}

	@Override
	public String getDocument(String url) {
		if (!this.urlMap.containsKey(url)) {
			return null;
		}
		return this.urlMap.get(url).getBody();
	}
	
	public Long getLastModified(String url) {
		if (!this.urlMap.containsKey(url)) {
			return null;
		}
		return this.urlMap.get(url).getLastModified();
	}

	@Override
	public int addUser(String username, String password) {
		if (this.userMap.containsKey(username)) {
			return -1;
		}
		this.userMap.put(username, new UserData(password, this.userMap.size()));
		return this.userMap.size();
	}

	@Override
	public boolean getSessionForUser(String username, String password) {
		MessageDigest md;
		String passHash = "";
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
		    BigInteger bi = new BigInteger(1, md.digest());
			passHash = bi.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if (this.userMap.containsKey(username)) {
			return this.userMap.get(username).getPassHash().equals(passHash);
		}
		return false;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		this.userDb.close();
		this.urlDb.close();
		this.contentSeenDb.close();
		this.channelDb.close();
		this.channelSitesDb.close();
		this.javaCatalog.close();
		this.env.close();
	}

	@Override
	public int addChannel(String name, String creator, String path) {
		if (this.channelMap.containsKey(name)) {
			return -1;
		}
		this.channelMap.put(name, new ChannelData(creator, path, this.channelMap.size() + 1));
		return this.channelMap.size();
	}

	@Override
	public ChannelData getChannel(String name) {
		return this.channelMap.get(name);
	}
	
	public boolean containsChannel(String name) {
		return this.channelMap.containsKey(name);
	}
	
	public Set<String> getChannelNames() {
		return this.channelMap.keySet();
	}
	
	public List<String> getChannelSites(String name) {
		List<String> sites = new ArrayList<String>();
		for (String s: this.channelSitesMap.get(name)) {
			sites.add(s);
		}
		return sites;
	}
	
	public int addChannelSite(String name, String url) {
		if (!this.channelSitesMap.containsKey(name)) {
			this.channelSitesMap.put(name, new HashSet<String>());
		}
		Set<String> s = this.channelSitesMap.get(name);
		s.add(url);
		this.channelSitesMap.put(name, s);
		return this.channelSitesMap.get(name).size();
	}
	
}
