package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent.Type;

public class XPathEngineImpl implements XPathEngine {
	// list of channel names
	List<String> channelNames;
	// list of paths split("/")
	List<List<String>> paths;
	// map of url to list of where they are in the path
	Map<String, List<Integer>> match;
	Map<String, Integer> currDepth;
	
	
	public XPathEngineImpl() {
		channelNames = new ArrayList<String>();
		paths = new ArrayList<List<String>>();
		match = new HashMap<String, List<Integer>>();
		currDepth = new HashMap<String, Integer>();
	}

	@Override
	public void setXPaths(String name, String expression) {
		if (this.isValid(expression)) {
			String[] path = expression.substring(1, expression.length()).split("/");
			channelNames.add(name);
			paths.add(Arrays.asList(path));
		}
	}

	@Override
	public boolean[] evaluateEvent(String url, OccurrenceEvent event) {
		// TODO Auto-generated method stub
		if (!this.match.containsKey(url)) {
			ArrayList<Integer> status = new ArrayList<Integer>();
			for (int i = 0; i < channelNames.size(); i++) {
				status.add(0);
			}
			this.match.put(url, status);
		}
		List<Integer> status = this.match.get(url);
		Type t = event.getType();
		String val = event.getValue();
		//updated status
		ArrayList<Integer> pos = new ArrayList<Integer>();
		
		//update the current depth of the URL 
		if (this.currDepth.containsKey(url)) {
			if (t == Type.Open) {
				this.currDepth.put(url, this.currDepth.get(url) + 1);
			} else if (t == Type.Close) {
				this.currDepth.put(url, this.currDepth.get(url) - 1);
			}
		} else {
			this.currDepth.put(url, 0);
		}
		
		for (int i = 0; i < status.size(); i++) {
			int pathLoc = status.get(i);
			if (pathLoc >= paths.get(i).size()) {
				pos.add(pathLoc);
				continue;
			}
			String currPath = paths.get(i).get(pathLoc);
			String param = null;
			if (currPath.contains("[")) {
				String[] pathParams = currPath.split("[");
				currPath = pathParams[0];
				if (pathParams.length > 1) {
					param = pathParams[1];
				}
			}
			if (t == Type.Open) {
				// path hasn't been matched
				// path is equivalent paths.get(i) = sample[contains(text(), "")]
				// path is in the same level
				if (val.equals(currPath.trim()) && pathLoc == this.currDepth.get(url)) {
					pos.add(pathLoc + 1);
				} else {
					pos.add(pathLoc);
				}
			} else if (t == Type.Close) {
				// if the tag closes a path
				if (val.equals(currPath.trim()) && pathLoc == this.currDepth.get(url)) {
					pos.add(pathLoc - 1);
				} else {
					pos.add(pathLoc);
				}
			} else {
				if(param != null && !param.equals("")) {
					String text = getTextParam(param);
					if (param.contains("contains")) {
						if (!val.contains(text)) {
							pos.add(pathLoc - 1);
						} else { 
							pos.add(pathLoc);
						}
					} else {
						if (!val.equals(text)) {
							pos.add(pathLoc - 1);
						} else {
							pos.add(pathLoc);
						}
					}
				} else {
					pos.add(pathLoc);
				}
			}
		}
		this.match.put(url, pos);
		// return a boolean array of whether the xpath is completed
		boolean[] xpaths = new boolean[pos.size()];
		for (int i = 0; i < pos.size(); i++) {
			if (pos.get(i) >= paths.get(i).size()) {
				xpaths[i] = true;
			} else {
				xpaths[i] = false;
			}
		}
		return xpaths;
	}
	
	public static String getTextParam(String path) {
		Pattern p = Pattern.compile("([\"])(.*)([\"])");
		Matcher m = p.matcher(path);
		while(m.find()) {
			String s = m.group();
			return s.substring(1, s.length() - 1);
		}
		return "hi";
	}
	
	public static boolean isValid(String path) {
		Pattern p = Pattern.compile("((\\/[\\w]*)([\\[].*[\\]])?)+");
		Matcher m = p.matcher(path);
		return m.matches();
	}
	
}
