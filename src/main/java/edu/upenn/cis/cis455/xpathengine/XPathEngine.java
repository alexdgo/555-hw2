package edu.upenn.cis.cis455.xpathengine;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface XPathEngine {
    /**
     * Sets the XPath expression(s) that are to be evaluated.
     * 
     * @param expressions
     */
    public void setXPaths(String name, String expression);

    /**
     * Event driven pattern match.
     * 
     * Takes an event at a time as input
     *
     * @param event notification about something parsed, from a given document
     * 
     * @return bit vector of matches to XPaths
     */
    public boolean[] evaluateEvent(String url, OccurrenceEvent event);
    
    public static boolean isValid(String path) {
		Pattern p = Pattern.compile("((\\/[\\w]*)([\\[].*[\\]])?)+");
		Matcher m = p.matcher(path);
		return m.matches();
	}

}
