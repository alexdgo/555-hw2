package edu.upenn.cis.stormlite.spout;

import java.io.BufferedReader;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;

public class CrawlerSpout implements IRichSpout{
	static Logger log = LogManager.getLogger(CrawlerSpout.class);

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the Crawler, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
	SpoutOutputCollector collector;
    BufferedReader reader;
    Fields schema = new Fields("url");
    private Crawler cm;
    private BlockingQueue<String> urls;
    
    public CrawlerSpout() {
    	log.debug("Start spout without params");
    	this.cm = Crawler.getCrawler();
    	this.urls = this.cm.getUrls();
    }

    public CrawlerSpout(Crawler cm) {
    	log.debug("Starting spout");
    	this.cm = cm;
    	this.urls = this.cm.getUrls();
    }

	@Override
	public String getExecutorId() {
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);
		
	}

	@Override
	public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextTuple() {
		this.cm.setWorking(true);
		if (!this.urls.isEmpty()) {
			this.collector.emit(new Values(this.urls.poll()));
		} else {
			Thread.yield();
		}
		this.cm.setWorking(false);
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
		
	}

}
