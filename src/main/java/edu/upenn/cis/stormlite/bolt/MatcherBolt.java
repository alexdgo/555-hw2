package edu.upenn.cis.stormlite.bolt;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.UrlData;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent.Type;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;

public class MatcherBolt implements IRichBolt{
	String executorId = UUID.randomUUID().toString();
	OutputCollector collector;
	Fields schema = new Fields("url");
	private StorageInterface db;
	private Crawler cm;
	private XPathEngine xpath;
	private List<String> channelNames;
	
	public MatcherBolt() {
		this.cm = Crawler.getCrawler();
		this.db = this.cm.getDb();
		this.xpath = XPathEngineFactory.getXPathEngine();
		this.channelNames = this.cm.getChannelNamesList();
	}
	
	public MatcherBolt(Crawler cm) {
		this.cm = cm;
		this.db = this.cm.getDb();
		this.xpath = XPathEngineFactory.getXPathEngine();
		this.channelNames = this.cm.getChannelNamesList();
	}
	
	@Override
	public String getExecutorId() {
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);
		
	}

	@Override
	public void cleanup() {
	}

	@Override
	public void execute(Tuple input) {
		this.cm.setWorking(true);
		String url = input.getStringByField("url");
		String type = input.getStringByField("elementType");
		String value = input.getStringByField("value");
		Type t;
		if (type.equals("Open")) {
			t = Type.Open;
		} else if (type.equals("Close")) {
			t = Type.Close;
		} else {
			t = Type.Text;
		}
		OccurrenceEvent query = new OccurrenceEvent(t, value);
		boolean[] channels = xpath.evaluateEvent(url, query);
//		System.out.println(url + " " + value + " " + channels[0] + channels[1] + channels[2] + channels[3]);
		for (int i = 0; i < this.channelNames.size(); i++) {
			if (channels[i]) {
				String cname = this.channelNames.get(i);
				int id = this.db.addChannelSite(cname, url);
			}
		}
		this.cm.setWorking(false);
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector=collector;
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
		
	}

	@Override
	public Fields getSchema() {
		return schema;
	}

}
