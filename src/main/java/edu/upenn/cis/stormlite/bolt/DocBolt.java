package edu.upenn.cis.stormlite.bolt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DocBolt implements IRichBolt{
	String executorId = UUID.randomUUID().toString();
	OutputCollector collector;
	private Fields schema = new Fields("url", "doc");
	private StorageInterface db;
	private Crawler cm;
	private Map<String, List<String>> disallow;
	private Map<String, Integer> delay;
	
	public DocBolt() {
    	this.cm = Crawler.getCrawler();
		this.db = this.cm.getDb();
		this.disallow = new HashMap<String, List<String>>();
		this.delay = new HashMap<String, Integer>();
    }
	
	public DocBolt(Crawler cm) {
		this.cm = cm;
		this.db = this.cm.getDb();
		this.disallow = new HashMap<String, List<String>>();
		this.delay = new HashMap<String, Integer>();
	}

	@Override
	public String getExecutorId() {
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		this.disallow.clear();
		this.delay.clear();
	}
	
	private String getText(String url) throws IOException {
	    HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
	    System.out.println(url);
	    
	    // handle error response code it occurs
	    int responseCode = connection.getResponseCode();
	    System.out.println(responseCode);
	    InputStream inputStream;
	    if (200 <= responseCode && responseCode <= 399) {
	        inputStream = connection.getInputStream();
	    } else {
	        inputStream = connection.getErrorStream();
	    }

	    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

	    StringBuilder response = new StringBuilder();
	    String currentLine;
	    boolean notFirstLine = false;
	    while ((currentLine = in.readLine()) != null) {
	    	if (notFirstLine) response.append("\r\n");
	    	response.append(currentLine);
	    	notFirstLine = true;
	    }
	    in.close();
	    return response.toString();
	}
	
	public String robotsUrl(String url) {
		URLInfo urlinfo = new URLInfo(url);
		return urlinfo.isSecure() ? "https://" + urlinfo.getHostName() + "/robots.txt"
				: "http://" + urlinfo.getHostName() + "/robots.txt";
	}
	
	public void robots(String url){
		URLInfo urlinfo = new URLInfo(url);
		try {
			String newUrl = robotsUrl(url);
			if (this.disallow.containsKey(newUrl) || this.delay.containsKey(newUrl)) {
				return;
			}
			HttpURLConnection connection = (HttpURLConnection) 
					new URL(newUrl).openConnection();
			InputStream inputStream = connection.getInputStream();
		    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		    StringBuilder response = new StringBuilder();
		    String currentLine;
		    boolean read = false;
		    ArrayList<String> dis = new ArrayList<String>();
		    while ((currentLine = in.readLine()) != null) {
		    	if (currentLine.equals("User-agent: *") 
		    			|| currentLine.equals("User-agent: cis455crawler")) {
		    		read = true;
		    	}
		    	if (read && currentLine.startsWith("Disallow:")) {
		    		dis.add(currentLine.substring(10));
		    	}
		    	if (read && currentLine.startsWith("Crawl-delay:")) {
		    		this.delay.put(newUrl, Integer.parseInt(currentLine.substring(13)));
		    	}
		    	if (read && currentLine.startsWith("User-agent:") &&
		    			!(currentLine.equals("User-agent: *") 
				    	|| currentLine.equals("User-agent: cis455crawler"))) {
		    		read = false;
		    	}
		    	response.append(currentLine);
		    }
		    this.disallow.put(newUrl, dis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void execute(Tuple input) {
		this.cm.setWorking(true);
		String in = input.getStringByField("url");
		// take in url, write to berkleydb
		try {
			if (this.cm.getNumDocs() >= this.cm.getMaxDocs()) return;

			// check size in mb
			URL url = new URL(in);
			HttpURLConnection urlc = (HttpURLConnection)url.openConnection();
			urlc.setRequestProperty("User-Agent", "cis455crawler");
			urlc.setRequestMethod("HEAD");
			long fileSize = urlc.getContentLength();
			if (urlc.getContentLength() > 0 && fileSize > (long)this.cm.getMaxSize() * 1024*1024) {
				return;
			}
			
			// check robots.txt
			this.robots(in);
			URLInfo urlinfo = new URLInfo(in);
			String path = urlinfo.getFilePath();
			if (this.disallow.containsKey(this.robotsUrl(in))) {
				for (String s: this.disallow.get(this.robotsUrl(in))) {
					if (path.startsWith(s)) return;
				}
			}
			//last accessed less than crawl-delay seconds ago
			String newUrl = this.robotsUrl(in);
			if (this.cm.getLastAccessed().containsKey(newUrl)) {
				int lastAccess = (int) (System.currentTimeMillis() - this.cm.getLastAccessed()
						.get(newUrl))/1000;
				if (this.delay.containsKey(newUrl) && lastAccess < this.delay.get(newUrl) ) {
					this.cm.getUrls().add(in);
					return;
				}
			}
			this.cm.getLastAccessed().put(this.robotsUrl(in), System.currentTimeMillis());
			
			String body = getText(in);
			// add to db
			int bodyId = this.cm.getDb().addDocument(in, body);
			// check if content has been seen already
			if (bodyId == -1) return;
			this.collector.emit(new Values(in, this.db.getDocument(in)));
			
			this.cm.incCount();
//			if (urlc.getContentType().contains("text/html")) {
				// bfs
				Document doc = Jsoup.connect(url.toString()).get();
				Elements links = doc.select("a[href]");
				for (Element link: links) {
					this.cm.getUrls().add(link.attr("abs:href"));
				}
//			}
		} catch (MalformedURLException e) {
			System.err.println("Invalid URL");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Can not open URL");
			e.printStackTrace();
		}
		this.cm.setWorking(false);
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
		
	}

	@Override
	public Fields getSchema() {
		return schema;
	}

}
