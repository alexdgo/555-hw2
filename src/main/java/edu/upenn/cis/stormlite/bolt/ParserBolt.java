package edu.upenn.cis.stormlite.bolt;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ParserBolt implements IRichBolt{
	static Logger log = LogManager.getLogger(ParserBolt.class);
	String executorId = UUID.randomUUID().toString();
	OutputCollector collector;
	private Crawler cm;
	Fields schema = new Fields("url", "elementType", "value");
	
	public ParserBolt() {
    	log.debug("Starting ParserBolt");
		this.cm = Crawler.getCrawler();
    }
	
	public ParserBolt(Crawler cm) {
		this.cm = cm;
	}

	@Override
	public String getExecutorId() {
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);
		
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub		
	}
	
	private void DFS(String url, Element c) {
		this.collector.emit(new Values(url, "Open", c.tagName()));
		if (c.children().size() > 0) {
			for (Element child: c.children()) {
				this.DFS(url, child);
			}
		}
		if (c.ownText() != null && !c.ownText().equals("")) {
			this.collector.emit(new Values(url, "Text", c.ownText()));
		}
		this.collector.emit(new Values(url, "Close", c.tagName()));
	}

	@Override
	public void execute(Tuple input) {
		this.cm.setWorking(true);
		String url = input.getStringByField("url");
		String body = input.getStringByField("doc");
		Document d = Jsoup.parse(body);
		Element root = d.child(0);
		this.DFS(url, root);
		this.cm.setWorking(false);
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
		
	}

	@Override
	public Fields getSchema() {
		return schema;
	}

}
