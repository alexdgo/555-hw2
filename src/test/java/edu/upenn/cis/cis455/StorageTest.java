package edu.upenn.cis.cis455;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

import edu.upenn.cis.cis455.storage.StorageImpl;

public class StorageTest {
	@Test
	public void addDocTest() {
		StorageImpl db = new StorageImpl("out");
		db.addDocument("https://www.example.com", "<html><body>hi</body></html>");
		assertEquals("<html><body>hi</body></html>", db.getDocument("https://www.example.com"));
	}
	
	@Test
	public void addUserTest() {
		StorageImpl db = new StorageImpl("out");
		db.addUser("y", "z");
		assertTrue(db.getSessionForUser("y", "z"));
		assertFalse(db.getSessionForUser("y", "y"));
	}
	
	@Test
	public void addUserTwiceTest() {
		StorageImpl db = new StorageImpl("out");
		db.addUser("y", "z");
		assertEquals(-1, db.addUser("y", "z"));
	}
}
