package edu.upenn.cis.cis455;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.CrawlerWorker;
import edu.upenn.cis.cis455.storage.StorageImpl;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class CrawlerTest {
	@Test
	public void testRobotParsing() throws IOException {
		String url = "https://en.wikipedia.org/wiki/Taylor_Swift";
		CrawlerWorker c = new CrawlerWorker(new Crawler(url, null, 100, 100), url);
		c.robots(url);
		List<String> out = c.getDisallow(c.robotsUrl(url));
		assertTrue(out.contains("/w/")); 
	}
	
	@Test
	public void testBasicCrawler() throws IOException {
		String url = "https://en.wikipedia.org/wiki/Taylor_Swift";
		if (!Files.exists(Paths.get("test"))) {
            try {
                Files.createDirectory(Paths.get("test"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		StorageImpl db = new StorageImpl("test");
		Crawler c = new Crawler(url, db, 100, 2);
		c.start();
		String body = (c.getDb().getDocument(url)); 
		assertTrue(body.startsWith("<!DOCTYPE html><html class=\"client-nojs\""));
	}
}
