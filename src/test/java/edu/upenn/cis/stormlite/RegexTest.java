package edu.upenn.cis.stormlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import edu.upenn.cis.cis455.xpathengine.XPathEngineImpl;

public class RegexTest {
	@Test
	public void XPathIsValidTest() {
		assertTrue(XPathEngineImpl.isValid("/rss/channel/title[contains(text(),”sports”)]"));
		assertTrue(XPathEngineImpl.isValid("/a/b/c"));
		assertTrue(XPathEngineImpl.isValid("/a/b[text() = \"hi\"]/c[text() = \"hello\"]"));
	}
	
	@Test
	public void XPathGetText() {
		assertEquals("hello", XPathEngineImpl.getTextParam("[text() = \"hello\"]"));
		assertEquals("hello", XPathEngineImpl.getTextParam("contains(text(), \"hello\")"));
	}
}
